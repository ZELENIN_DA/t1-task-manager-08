package ru.t1.dzelenin.tm.repository;

import ru.t1.dzelenin.tm.constant.ArgumentConst;
import ru.t1.dzelenin.tm.constant.CommandConst;
import ru.t1.dzelenin.tm.model.Command;

public class CommandRepository {

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show application version");

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, " Show developer info");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Exit application");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info");

    public static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show command list");

    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show argument list");

    public static final Command[] TERMINAL_COMMANDS = new Command[]{ABOUT, VERSION, HELP, INFO, COMMANDS, ARGUMENTS, EXIT};

    public static Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
