package ru.t1.dzelenin.tm.constant;

public final class CommandConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String INFO = "info";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    private CommandConst() {
    }

}
