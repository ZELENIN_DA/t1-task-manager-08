package ru.t1.dzelenin.tm;

import ru.t1.dzelenin.tm.constant.ArgumentConst;

import ru.t1.dzelenin.tm.constant.CommandConst;
import ru.t1.dzelenin.tm.model.Command;
import ru.t1.dzelenin.tm.repository.CommandRepository;
import ru.t1.dzelenin.tm.util.FormatUtil;

import java.util.Scanner;

import static java.lang.Runtime.getRuntime;
import static ru.t1.dzelenin.tm.constant.CommandConst.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
        System.exit(0);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.INFO:
                showInfo();
                break;
            case CommandConst.COMMANDS:
                showCommands();
                break;
            case CommandConst.ARGUMENTS:
                showArguments();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = getRuntime().maxMemory();
        final String maxMemoryFormat = (FormatUtil.formatBytes(maxMemory));
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? ("no limit") : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + FormatUtil.formatBytes(usageMemory));
    }

    private static void exit() {
        System.exit(0);
    }

    public static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    public static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(2);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Daniil Zelenin");
        System.out.println("email: dzelenin@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
    }

}
